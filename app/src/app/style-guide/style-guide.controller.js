/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.style-guide')
        .controller('StyleGuideController', StyleGuideController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    StyleGuideController.$inject = [
        // Angular
        '$log',
        '$scope'
    ];

    function StyleGuideController(
        // Angular
        $log,
        $scope
    ) {
        // ViewModel
        // =========
        var vm = this;

        // Interaction
        // -----------
        vm.$$ix = {
            cancel: cancelAction,
            save  : saveAction
        };

        // User Interface
        // --------------
        vm.$$ui = {
            categories: [
                { id: 0, name: faker.lorem.words(1).shift() },
                { id: 1, name: faker.lorem.words(1).shift() },
                { id: 2, name: faker.lorem.words(1).shift() },
                { id: 3, name: faker.lorem.words(1).shift() },
                { id: 4, name: faker.lorem.words(1).shift() }
            ],
            tags: [
                { id: 0, name: faker.lorem.words(1).shift(), selected: false },
                { id: 1, name: faker.lorem.words(1).shift(), selected: false },
                { id: 2, name: faker.lorem.words(1).shift(), selected: false },
                { id: 3, name: faker.lorem.words(1).shift(), selected: false },
                { id: 4, name: faker.lorem.words(1).shift(), selected: false }
            ],
            title: 'Style Guide'
        };

        // Data
        // ----
        vm.post = {
            title: null,
            content: null,
            category: null,
            tags: []
        };

        // Functions
        // =========
        // Actions
        // -------
        function saveAction() {
            $log.log('saveAction:', vm.post);
        }

        function cancelAction() {
            $log.log('cancelAction');
        }

        // Watchers
        // --------
        $scope.$watch('vm.ui.tags', tagsWatcher, true);
        function tagsWatcher(newValue, oldValue) {
            vm.post.tags = _.map(_.filter(newValue, { selected: true }), 'id');
        }
    }

})();
