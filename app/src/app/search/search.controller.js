/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.search')
        .controller('SearchController', SearchController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    SearchController.$inject = ['$log','$scope','$http','$state'];

    function SearchController($log, $scope, $http, $state) {
        // ViewModel
        var vm = this;

        // User Interface
        vm.$$ui = {
            classname: 'search'
        }

        var data_to_fill = [];
        var params = { searchstring: $state.params.searchstring };

        $http.get('http://95.85.26.250:3000/api/events/search').success(function(data) {

            var parameters = params.searchstring.toLowerCase().split("&");
            var data_to_fill_search_ = [];
            var data_to_fill_search = [];

            // Loop the events
            for(var eventsLoop = 0; eventsLoop < data.length; eventsLoop++)
            {
                // Make an array of keywords for each event
                var eventKeywords = [];

                for(var i = 0; i < data[eventsLoop].name.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].name.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].keywords.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].keywords.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].address.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].address.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].description.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].description.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].first_name.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].first_name.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].last_name.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].last_name.split(" ")[i].toLowerCase());

                for(var i = 0; i < data[eventsLoop].username.split(" ").length; i++)
                    eventKeywords.push(data[eventsLoop].username.split(" ")[i].toLowerCase());

                // Now search each word for matches with your seach
                for (var search = 0; search < parameters.length; search++)
                {
                    for (var keyword = 0; keyword < eventKeywords.length; keyword++)
                    {
                        if (parameters[search] == eventKeywords[keyword])
                        {
                            data_to_fill_search_.push(data[eventsLoop]);
                        }
                    }
                }

                // Find and remove duplicates
                data_to_fill_search = jQuery.unique(data_to_fill_search_);

            }

            document.querySelector('.header__eventinfo h1').innerHTML = "Zoeken naar " + params.searchstring;

            // if nothing
            if (data_to_fill_search_.length == 0)
            {
                console.log('nothing');
                document.querySelector('.nothing').innerHTML = "Sorry, geen event gevonden. Probeer het eens met andere sleutelwoorden.";
            }

            $scope.events_search = data_to_fill_search;

            for (var i = 0; i < 4; i++)
                data_to_fill.push(data[(data.length - 1) - i]);
            $scope.latest_events = data_to_fill;

        });

    }

})();
