/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let concat     = require('gulp-concat'),
        gulp_if    = require('gulp-if'),
        ngAnnotate = require('gulp-ng-annotate'),
        uglify     = require('gulp-uglify');

    gulp.task('scripts', [
        'scripts:app'
    ]);

    gulp.task('scripts:app', () => {
        return gulp.src(`${CONFIG.dir.src}app/**/*.js`)
            .pipe(concat('app.js', CONFIG.concat))
            .pipe(ngAnnotate(CONFIG.ngAnnotate))
            .pipe(gulp_if(isProduction, uglify(CONFIG.uglify)))
            .pipe(gulp.dest(`${CONFIG.dir.dest}js/`));
    });

})(require('gulp'));
