@extends('overview.app')

@section('content')

<?php

    if (!isset($_COOKIE["loggedIn"]))
    {
        header('Location:/');
        exit;
    }
    
    if (isset($_POST['user_id']))
    {
        DB::table('tickets')
            ->where('id', $_POST['id'])
            ->update(
                array(
                    'event_id'    =>  $_POST['event_id'],
                    'user_id'  =>  $_POST['user_id'],
                    'amount'   =>  $_POST['amount']
                )
            );

        header('Location:/tickets');
    }

    if (isset($_POST['delete_id']) && !isset($_POST['username']))
    {
        DB::table('tickets')
            ->where('id', $_POST['delete_id'])
            ->update(array('deleted_at' => date('Y-m-d h:i:s')));

        header('Location:/tickets');
    }

    if (isset($_POST['new_user_id']))
    {
        DB::table('tickets')
            ->insert(
                array(
                    'user_id'  => $_POST['new_user_id'],
                    'event_id' => $_POST['new_event_id'],
                    'amount'   => $_POST['new_amount'],
                )
            );

        header('Location:/tickets');
    }
?>

    <h1>Create ticket</h1>
    <table>
        <tr>
            <td>User ID</td>
            <td>Event ID</td>
            <td>Amount</td>
            <td></td>
        </tr>
        {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                <td><?=Form::number('new_user_id', '', array('required', 'style' => 'width: 100px', 'placeholder' => 'User ID'));?></td>
                <td><?=Form::number('new_event_id', '', array('required', 'style' => 'width: 100px', 'placeholder' => 'Event ID'));?></td>
                <td><?=Form::number('new_amount', '', array('required', 'placeholder' => 'Amount'));?></td>
                <td><?=Form::submit('Add');?></td>
            </tr>
        {!! Form::close() !!}
    </table>


    <h1>Tickets - <a href="/api/tickets">API</a></h1>
<?php
        $tickets = DB::table('tickets')
            ->join('users', 'tickets.user_id', '=', 'users.id')
            ->join('events', 'tickets.event_id', '=', 'events.id')
            ->select('tickets.id', 'tickets.user_id', 'tickets.event_id', 'users.username', 'events.name', 'tickets.amount')
            ->where('tickets.deleted_at', '=', NULL)
            ->get();
?>

        <table class="view">
            <tr>
                <td>ID</td>
                <td>Username</td>
                <td>Event</td>
                <td>Amount</td>
                <td></td>
                <td></td>
            </tr>

<?php foreach ($tickets as $ticket) : ?>
            {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                <tr>
                    <td style="display: none;"><?=Form::text('id', $ticket->id, array('required'));?></td>
                    <td><?=$ticket->id;?></td>
                    <td>
                        <?=Form::number('user_id', $ticket->user_id, array('required', 'style' => 'width: 45px'));?>
                        ( <?=$ticket->username;?> )
                    </td>
                    <td>
                        <?=Form::number('event_id', $ticket->event_id, array('required', 'style' => 'width: 45px'));?>
                        ( <?=$ticket->name;?> )
                    </td>
                    <td><?=Form::number('amount', $ticket->amount, array('required'));?></td>
                    <td><?=Form::submit('Update');?></td>
            {!! Form::close() !!}
            {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                    <td>
                        <p style="display: none;"><?=Form::text('delete_id', $ticket->id, array('required'));?></p>
                        <?=Form::submit('Delete');?>
                    </td>
            {!! Form::close() !!}
                </tr>
<?php endforeach; ?>
        </table>

@endsection
