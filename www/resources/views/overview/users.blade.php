@extends('overview.app')

@section('content')

<?php

    if (!isset($_COOKIE["loggedIn"]))
    {
        header('Location:/');
        exit;
    }

    if (isset($_POST['username']))
    {
        $check_user = DB::table('users')->where('id', '=', $_POST['id'])->get();
        if (COUNT($check_user) != 0)
        {
            DB::table('users')
                ->where('id', $_POST['id'])
                ->update(
                        array(
                            'username'    =>  $_POST['username'],
                            'first_name'  =>  $_POST['first_name'],
                            'last_name'   =>  $_POST['last_name'],
                            'email'       =>  $_POST['email'],
                            'updated_at'  =>  date('Y-m-d h:i:s')
                        )
                );

            if ($_POST['password'] != '')
            {
                DB::table('users')
                    ->where('id', $_POST['id'])
                    ->update(
                            array(
                                'password' => md5($_POST['password'])
                            )
                    );
            }

            // avatar upload
            if ($_FILES['avatar']['name'] != "")
            {
                $file = $_FILES['avatar'];
                $destinationPath = 'pictures/';

                // Get a unique name
                $filename = md5($_POST['username'] . $_POST['id'] . rand(1000, 9999)) . '.jpg';
                $filename = str_replace(' ', '_', $filename);
                move_uploaded_file($_FILES['avatar']['tmp_name'], $destinationPath . $filename);

                DB::table('users')
                    ->where('id', $_POST['id'])
                    ->update(array('avatar' =>  $filename));
            }

            header('Location:/users');

        }
        else
        {
            echo "<span style='color: red;'><h1>Unknown error.</h1></span>";
        }
    }

    if (isset($_POST['delete_id']) && !isset($_POST['username']))
    {
        DB::table('users')
            ->where('id', $_POST['delete_id'])
            ->update(array('deleted_at' => date('Y-m-d h:i:s')));

        header('Location:/users');
    }

    if (isset($_POST['new_username']))
    {
        DB::table('users')
            ->insert(
                array(
                    'username'    =>  $_POST['new_username'],
                    'password'    =>  md5($_POST['new_password']),
                    'first_name'  =>  $_POST['new_first_name'],
                    'last_name'   =>  $_POST['new_last_name'],
                    'email'       =>  $_POST['new_email'],
                    'avatar'      =>  'empty_avatar.png',
                    'created_at'  =>  date('Y-m-d h:i:s')
                )
            );

        // avatar upload
        if ($_FILES['new_avatar']['name'] != "")
        {
            $file = $_FILES['new_avatar'];
            $destinationPath = 'pictures/';

            // Get a unique name
            $filename = md5($_POST['new_username'] . rand(1000, 9999)) . '.jpg';
            $filename = str_replace(' ', '_', $filename);
            move_uploaded_file($_FILES['new_avatar']['tmp_name'], $destinationPath . $filename);

            DB::table('users')
                ->where('username', $_POST['new_username'])
                ->update(array('avatar' =>  $filename));
        }

        header('Location:/users');
    }
?>

    <h1>Create user</h1>
    <table>
        <tr>
            <td>Username</td>
            <td>Password</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>E-mail</td>
            <td>Avatar</td>
            <td></td>
        </tr>
        {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
            <tr>
                <td><?=Form::text('new_username', '', array('required', 'placeholder' => 'Username'));?></td>
                <td><?=Form::password('new_password', '', array('required', 'placeholder' => 'Password'));?></td>
                <td><?=Form::text('new_first_name', '', array('required', 'placeholder' => 'First Name'));?></td>
                <td><?=Form::text('new_last_name', '', array('required', 'placeholder' => 'Last Name'));?></td>
                <td><?=Form::text('new_email', '', array('required', 'placeholder' => 'Email'));?></td>
                <td><?=Form::file('new_avatar', '', array('required'));?></td>
                <td><?=Form::submit('Add');?></td>
            </tr>
        {!! Form::close() !!}
    </table>


    <h1>Users - <a href="/api/users">API</a></h1>
<?php $users = DB::table('users')->where('deleted_at', '=', NULL)->get(); ?>

    <table class="view">
        <tr>
            <td>ID</td>
            <td>Username</td>
            <td>Password</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>E-mail</td>
            <td>Avatar</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
<?php foreach ($users as $user) : ?>
        {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
            <tr>
                <td style="display: none;"><?=Form::text('id', $user->id, array('required'));?></td>
                <td><?=$user->id;?></td>
                <td><?=Form::text('username', $user->username, array('required', 'placeholder' => 'Username'));?></td>
                <td><?=Form::password('password', '', array());?></td>
                <td><?=Form::text('first_name', $user->first_name, array('required', 'placeholder' => 'First Name'));?></td>
                <td><?=Form::text('last_name', $user->last_name, array('required', 'placeholder' => 'Last Name'));?></td>
                <td><?=Form::text('email', $user->email, array('required', 'placeholder' => 'Email'));?></td>
                <td><a href="pictures/<?=$user->avatar;?>" target="_blank"><img src="pictures/<?=$user->avatar;?>"></a></td>
                <td><?=Form::file('avatar', array('placeholder' => 'Avatar'));?></td>
                <td><?=Form::submit('Update');?></td>
        {!! Form::close() !!}
        {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                <td>
                    <p style="display: none;"><?=Form::text('delete_id', $user->id, array('required'));?></p>
                    <?=Form::submit('Delete');?>
                </td>
        {!! Form::close() !!}
            </tr>
<?php endforeach; ?>
    </table>

@endsection
