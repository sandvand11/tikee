@extends('overview.app')

@section('content')

<?php
    if (isset($_GET['username']))
    {
        if ($_GET['username'] == 'admin' && $_GET['password'] == 'tikee')
        {
            setcookie("loggedIn", true);
            header("Location:/users");
            exit;
        }
    }

?>

    <h1>Please log in</h1>

    {!! Form::open(['method' => 'get']) !!}
        <table>
            <tr>
                <td>Login</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><?=Form::text('username', '', array('required'));?></td>
                <td></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><?=Form::password('password', '', array('required'));?></td>
                <td><?=Form::submit('Log in');?></td>
            </tr>
        </table>
    {!! Form::close() !!}

@endsection
