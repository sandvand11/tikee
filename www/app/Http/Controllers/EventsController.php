<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Event;
use DB;

class EventsController extends Controller
{
    public function getEvents()
    {
        $events = DB::table('events')
            ->where("deleted_at", "=", NULL)
            ->get();

        foreach($events as $event)
        {
            $response[] = [
                'id'            => $event->id,
                'user_id'       => $event->user_id,
                'name'          => $event->name,
                'datetime'      => $event->datetime,
                'time'          => $event->time,
                'price'         => $event->price,
                'description'   => $event->description,
                'address'       => $event->address,
                'website_url'   => $event->website_url,
                'keywords'      => $event->keywords,
                'image'         => $event->image
            ];
        }

        return $response;
    }

    public function search()
    {
    	$events = DB::table('events')
    		->join('users', 'events.user_id', '=', 'users.id')
            ->select(
                'events.id',
                'events.name',
                'events.description',
                'events.keywords',
                'events.address',
                'events.datetime',
                'events.time',
                'events.price',
                'events.image',
                'users.first_name',
                'users.last_name',
                'users.username'
            )
            ->where('events.deleted_at', '=', NULL)
    		->get();

    	return $events;
    }

    public function getEventsDesc()
    {
        $events = DB::table('events')
            ->where("deleted_at", "=", NULL)
            ->orderBy('id', 'desc')
            ->get();

        foreach($events as $event)
        {
            $response[] = [
                'id'            => $event->id,
                'user_id'       => $event->user_id,
                'name'          => $event->name,
                'datetime'      => $event->datetime,
                'time'          => $event->time,
                'price'         => $event->price,
                'description'   => $event->description,
                'address'       => $event->address,
                'website_url'   => $event->website_url,
                'keywords'      => $event->keywords,
                'image'         => $event->image
            ];
        }

        return $response;
    }

    public function getEventById($event_id)
    {
        $events = DB::table('events')
            ->where('id', $id)
            ->where("deleted_at", "=", NULL)
            ->get();
        return $events;
    }

    public function getEventsUser($user_id)
    {
        $events = DB::table('events')
            ->where('user_id', $user_id)
            ->where("deleted_at", "=", NULL)
            ->get();
        return $events;
    }

    public function getEventsTicketsUsers($event_id)
    {
        $data = DB::table('tickets')
            ->join('users', 'tickets.user_id', '=', 'users.id')
            ->where('tickets.event_id', $event_id)
            ->where("tickets.deleted_at", "=", NULL)
            ->select('users.first_name', 'users.last_name', 'users.id', 'users.username', 'users.avatar')
	    ->distinct('users.id')
	    ->get();
        return $data;
    }

    public function addEvent(Request $input)
    {
        // Upload image
        try
        {
            $this->validate($input, ['image' => 'required|image']);
            $destinationPath = 'pictures';
            $file = $input->file('image');

            if ($file->isValid())
            {
                // Get a unique name
                $filename = md5($input['name'] . rand(1000, 9999)) . '.' . $file->getClientOriginalExtension();
                $filename = str_replace(' ','_',$filename);
                $file->move($destinationPath, $filename);
            }
        }
        catch (Exception $ex)
        {
            Log::error($ex);
        }

        $event = new Event();
        $event->user_id = $input['user_id'];
        $event->name = $input['name'];
        $event->datetime = $input['datetime'];
        $event->time = $input['time'];
        $event->price = $input['price'];
        $event->description = $input['description'];
        $event->address = $input['address'];
        $event->website_url = $input['website_url'];
        $event->keywords = $input['keywords'];
        $event->image = "pictures/".$filename;
        $event->save();
        return json_encode($event);
    }
}
