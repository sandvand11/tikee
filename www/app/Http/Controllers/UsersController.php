<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use DB;

class UsersController extends Controller
{
    public function getUsers()
    {
        $events = DB::table('users')->get();
        return $events;
    }

    public function getUserById($id)
    {
        $events = DB::table('users')
            ->where('id', $id)
            ->get();
        return $events;
    }

    public function updateUser(Request $input, $user_id)
    {
    	$first_name = $input->first_name;
    	$last_name = $input->last_name;

    	DB::table('users')
            ->where('id', $user_id)
            ->update(array('first_name' => $first_name, 'last_name' => $last_name));

    	return DB::table('users')->where('id', $user_id)->get();
    }

    public function uploadAvatar(Request $input, $user_id)
    {
        try
        {
            $this->validate($input, [ 'avatar' => 'required|image' ]);
            $destinationPath = 'pictures';

            $user = User::find($user_id);
            $file = $input->file('avatar');

            if ($file->isValid())
            {
                // Get a unique name
                $filename = md5($user->username . $user_id . rand(1000, 9999)) . '.' . $file->getClientOriginalExtension();
                $filename = str_replace(' ','_',$filename);
                $file->move($destinationPath, $filename);

                DB::table('users')
                    ->where('id', $user_id)
                    ->update(array('avatar' => $filename));

                return 'Successfully uploaded.';
            }
        }
        catch (Exception $ex)
        {
            Log::error($ex);
            return 'Couldn\'t upload the picture.';
        }
    }

    public function registerUser(Request $input)
    {
        $user = new User();
        $user->username = $input['username'];
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->password = md5($input['password']);
        $user->email = $input['email'];
        $user->avatar = 'empty_avatar.png';
        $user->save();
        return json_encode($user);
    }

    public function loginUser(Request $input)
    {
        $user = DB::table('users')
            ->where('username', '=', $input['username'])
            ->get();

        if (!empty($user))
        {
            if($user[0]->password == md5($input['password']))
            {
                return $user;
            }
            else
            {
                return "Login failed";
            }
        }
        else
        {
            return "User not found";
        }       
    }
}
